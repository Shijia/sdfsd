# coding: utf-8

from django import forms
from DjangoUeditor.forms import UEditorField


class UserForm(forms.Form):
    
    username = forms.CharField(label=u'username', max_length=30)
    email = forms.EmailField(label=u'email')
    password1 = forms.CharField(label=u'password', widget=forms.PasswordInput())
    password2 = forms.CharField(label=u'confrimpassword', widget=forms.PasswordInput())
    
class ChangePwd(forms.Form):
    
    email = forms.EmailField(label=u'email�')
    oldpassword = forms.CharField(label=u'old password', widget=forms.PasswordInput())
    password1 = forms.CharField(label=u'new password', widget=forms.PasswordInput())
    password2 = forms.CharField(label=u'�confirm new passowrd', widget=forms.PasswordInput())
    
class PostsForm(forms.Form):
    title = forms.CharField(label=u'title', max_length=30)
    details = UEditorField(label=u'comment')

class SubpostForm(forms.Form):
    comments = UEditorField(label=u'reply')
