# coding: utf-8

from django.db import models
from DjangoUeditor.models import UEditorField

class MyUser(models.Model):
    username = models.CharField(max_length=50)
    userpassword = models.CharField(max_length=20)
    images = models.ImageField(upload_to='./heads/', blank=True) 
    email = models.EmailField(primary_key=True)
    date = models.DateTimeField(auto_now_add=True) 
    def __unicode__(self):
        return self.username

class Reply(models.Model):
    title = models.CharField(max_length=50)
    details = UEditorField(verbose_name=u'comment�')
    datetime = models.DateTimeField(auto_now_add=True)   
    user = models.ForeignKey(MyUser)    
    def __unicode__(self):
        return self.title
    
class Subject(models.Model):
    title = models.CharField(max_length=50)     
    details = UEditorField(verbose_name=u'comment�')    
    datetime = models.DateTimeField(auto_now_add=True)  
    reply = models.ManyToManyField(Reply)  
    user = models.ForeignKey(MyUser)  
    def __unicode__(self):
        return self.title


